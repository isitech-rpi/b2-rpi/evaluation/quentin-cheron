#include "EquipeMoto.h";

#include <iostream>
#include <string>

using namespace std;

namespace EquipeMotos
{
	EquipeMoto::EquipeMoto(std::string nom) {
		this->nom = nom;
	};

	EquipeMoto::~EquipeMoto() {
		std::cout << "Destruction de l'objet EquipeMoto";
	};
}