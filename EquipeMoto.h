#pragma once
#ifndef EQUIPEMOTO_H
#define EQUIPEMOTO_H

#include <string>
#include "personne.h";

using namespace std;
using namespace Personnes;

namespace EquipeMotos
{
	class EquipeMoto
	{
	private:
		std::string nom;

	public:
		int maxPilote = 3;

		EquipeMoto(std::string nom);
		virtual ~EquipeMoto();

		std::string getNom() const { return nom; };
		void setNom(std::string nom) { this->nom = nom; };

		// Personne* getManager() const {return Personne}

		char* AddPilote(int rang, Personne* pilote);
	};
}

#endif