#include "personne.h";

#include <iostream>
#include <string>

using namespace std;

namespace Personnes
{
	Personne::Personne(std::string nom) {
		this->nom = nom;
	};

	Personne::~Personne() {
		std::cout << "Destruction de l'objet Personne";
	};
}