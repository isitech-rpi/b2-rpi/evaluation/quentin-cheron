#pragma once
#ifndef PERSONNE_H
#define PERSONNE_H

#include <iostream>
#include <sstream>
#include <string>

namespace Personnes
{
	class Personne
	{
		private :
			std::string nom;

	public:
		Personne(std::string nom);
		virtual ~Personne();

		Personne* CreerPersonne(std::string nom) {
			Personne* pilote_1 = new Personne("Mama");
		};

		std::string AfficherPilote(Personne* pilote) {
			std::ostringstream oss;
			oss << "Personne(" << pilote << "): " + pilote->getNom();
			return oss.str();
		}

		std::string getNom() const { return nom; };
		void setNom(std::string nom) { this->nom = nom; };
	};
}

#endif